#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <cstrike>
#include <smlib>
#include <multicolors>

#define ZONETYPE_SHOOT 1
#define ZONETYPE_DMG 2

bool g_bAllowShooting[MAXPLAYERS + 1];
bool g_bAllowDamage[MAXPLAYERS + 1];

//If enabled use block at controls instead of set velocity to zero

enum struct MapZoneEditor
{
	int Step;
	float Point1[3];
	float Point2[3];
	int Level_Id;
	int Type;
	char Name[32];
}

enum struct MapZone
{
    int Id;
    int Type;
    char Map[32];
    float Point1[3];
    float Point2[3];
	int Level_Id;
	char zName[32];
}

int g_MapZoneEntityZID[2048];
bool g_bZone[128][MAXPLAYERS+1];

float g_fSpam[MAXPLAYERS+1];

/**
* Global Variables
*/
Handle g_hSQL;

char g_currentMap[64];
int g_reconnectCounter = 0;

MapZone g_mapZones[128];
int g_mapZonesCount = 0;

MapZoneEditor g_mapZoneEditors[MAXPLAYERS+1];
int precache_laser;

int g_Freeze;

public Plugin myinfo =
{
	name        = "Trade: Fight Zones",
	author      = "m_bNightstalker & Zipcore",
	description = "Creating different fight zones for trade servers.",
	version     = "1.0",
	url         = "www.zipcore.net"
};

public void OnPluginStart()
{
	g_Freeze = FindSendPropInfo("CBasePlayer", "m_fFlags");
	if(g_Freeze == -1)
		SetFailState("Couldnt find the m_fFlags offset!");
		
	RegAdminCmd("fz_add_shoot", Command_ZoneAddShoot, ADMFLAG_RCON);
	RegAdminCmd("fz_add_dmg", Command_ZoneAddDmg, ADMFLAG_RCON);
	RegAdminCmd("fz_delete", Command_DeleteZone, ADMFLAG_RCON);
	RegAdminCmd("fz_reload", Command_ReloadZones, ADMFLAG_RCON);
	
	HookEvent("round_start", Event_RoundStart, EventHookMode_Post);
	
	HookEvent("player_spawn", Event_PlayerReset);
	HookEvent("player_death", Event_PlayerReset);
	
	HookEvent("item_equip", Event_ItemEquip);
}

public Action Command_ZoneAddShoot(int client, int args)
{
	RestartMapZoneEditor(client);
	g_mapZoneEditors[client].Step = 1;
	g_mapZoneEditors[client].Type = ZONETYPE_SHOOT;
	DisplaySelectPointMenu(client, 1);
		
	return Plugin_Handled;	
}

public Action Command_ZoneAddDmg(int client, int args)
{
	RestartMapZoneEditor(client);
	g_mapZoneEditors[client].Step = 1;
	g_mapZoneEditors[client].Type = ZONETYPE_DMG;
	DisplaySelectPointMenu(client, 1);
		
	return Plugin_Handled;	
}

public Action StartTouchTrigger(int caller, int activator)
{
	if (activator < 1 || activator > MaxClients)
		return;
	
	if (!IsClientInGame(activator))
		return;
	
	if (!IsPlayerAlive(activator))
		return;
	
	int client = activator;
	
	int zone = g_MapZoneEntityZID[caller];
	
	if(zone < 0)
		return;
	
	g_bZone[zone][client] = true;
	
	g_bAllowShooting[client] = IsPlayerTouchingZoneType(client, ZONETYPE_SHOOT);
	g_bAllowDamage[client] = IsPlayerTouchingZoneType(client, ZONETYPE_DMG);
}

public Action EndTouchTrigger(int caller, int activator)
{
	if (activator < 1 || activator > MaxClients)
		return;
	
	if (!IsClientInGame(activator))
		return;
	
	if (!IsPlayerAlive(activator))
		return;
	
	int client = activator;
	
	int zone = g_MapZoneEntityZID[caller];
	
	if(zone < 0)
		return;
		
	g_bZone[zone][client] = false;
	
	g_bAllowShooting[client] = IsPlayerTouchingZoneType(client, ZONETYPE_SHOOT);
	g_bAllowDamage[client] = IsPlayerTouchingZoneType(client, ZONETYPE_DMG);
}

public void OnMapStart()
{
	for (int z = 0; z < 128; z++)
		for (int i = 0; i <= 64; i++)
			g_bZone[z][i] = false;
	
	GetCurrentMap(g_currentMap, sizeof(g_currentMap));
	ConnectSQL();
	
	precache_laser = PrecacheModel("materials/sprites/laserbeam.vmt");
	PrecacheModel("models/props_junk/wood_crate001a.mdl", true);
}

public Action Event_RoundStart(Handle event,const char[] name,bool dontBroadcast)
{
	for (int i = 0; i <= 64; i++)
	{
		g_bAllowShooting[i] = false;
		g_bAllowDamage[i] = false;
		
		for (int z = 0; z < 128; z++)
				g_bZone[z][i] = false;
	}
	
	if (g_hSQL != null)
		LoadMapZones();
	else ConnectSQL();
}

public void OnClientPutInServer(int client)
{
	g_fSpam[client] = GetGameTime();
	
	for (int i = 0; i < 128; i++)
		g_bZone[i][client] = false;
		
	g_bAllowShooting[client] = false;
	g_bAllowDamage[client] = false;
		
	SDKHook(client, SDKHook_OnTakeDamage, Hook_OnTakeDamage);
}

public Action Event_PlayerReset(Handle event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	g_fSpam[client] = GetGameTime();
	
	for (int i = 0; i < 128; i++)
		g_bZone[i][client] = false;
		
		
	g_bAllowShooting[client] = false;
	g_bAllowDamage[client] = false;
}

void AddMapZone(char[] map, int type, char[] name, int level_id, float point1[3], float point2[3])
{
	char query[512];
	FormatEx(query, sizeof(query), "INSERT INTO `fightzones` (map, type, name, level_id, point1_x, point1_y, point1_z, point2_x, point2_y, point2_z) VALUES ('%s','%d','%s','%d', %f, %f, %f, %f, %f, %f);", map, type, name, level_id, point1[0], point1[1], point1[2], point2[0], point2[1], point2[2]);
	SQL_TQuery(g_hSQL, MapZoneChangedCallback, query, _, DBPrio_Normal);	
}

public int MapZoneChangedCallback(Handle owner, Handle hndl, const char[] error, any data)
{
	if (hndl == null)
	{
		return;
	}
	
	LoadMapZones();
}

bool LoadMapZones()
{
	if (g_hSQL != null)
	{
		char query[512];
		FormatEx(query, sizeof(query), "SELECT * FROM `fightzones` WHERE map = '%s' ORDER BY level_id ASC;", g_currentMap);
		SQL_TQuery(g_hSQL, LoadMapZonesCallback, query, _, DBPrio_High);
		
		return true;
	}
	
	return false;
}


public LoadMapZonesCallback(Handle owner, Handle hndl, const char[] error, any data)
{
	if (hndl == null)
		return;
	
	g_mapZonesCount = 0;
	DeleteAllZoneEntitys();
	
	while (SQL_FetchRow(hndl))
	{
		strcopy(g_mapZones[g_mapZonesCount].Map, 64, g_currentMap);
		
		g_mapZones[g_mapZonesCount].Id = SQL_FetchInt(hndl, 0);
		g_mapZones[g_mapZonesCount].Type = SQL_FetchInt(hndl, 1);
		g_mapZones[g_mapZonesCount].Level_Id = SQL_FetchInt(hndl, 2);
		
		g_mapZones[g_mapZonesCount].Point1[0] = SQL_FetchFloat(hndl, 3);
		g_mapZones[g_mapZonesCount].Point1[1] = SQL_FetchFloat(hndl, 4);
		g_mapZones[g_mapZonesCount].Point1[2] = SQL_FetchFloat(hndl, 5);
		
		g_mapZones[g_mapZonesCount].Point2[0] = SQL_FetchFloat(hndl, 6);
		g_mapZones[g_mapZonesCount].Point2[1] = SQL_FetchFloat(hndl, 7);
		g_mapZones[g_mapZonesCount].Point2[2] = SQL_FetchFloat(hndl, 8);
		
		char ZoneName[32];
		SQL_FetchString(hndl, 10, ZoneName, sizeof(ZoneName));
		FormatEx(g_mapZones[g_mapZonesCount].zName, 32, "%s", ZoneName);
		
		SpawnZoneEntitys(g_mapZonesCount);
		
		g_mapZonesCount++;
	}
}

void ConnectSQL()
{
	if (g_hSQL != null)
		CloseHandle(g_hSQL);
	
	g_hSQL = null;
	
	if (SQL_CheckConfig("fightzones"))
		SQL_TConnect(ConnectSQLCallback, "fightzones");
	else SetFailState("PLUGIN STOPPED - Reason: no config entry found for 'fightzones' in databases.cfg - PLUGIN STOPPED");
}

public int ConnectSQLCallback(Handle owner, Handle hndl, const char[] error, any data)
{
	if (g_reconnectCounter >= 5)
	{
		SetFailState("PLUGIN STOPPED - Reason: reconnect counter reached max - PLUGIN STOPPED");
		return;
	}
	
	if (hndl == null)
	{
		LogError("[fightzones.smx] Connection to SQL database has failed, Try #%d, Reason: %s", g_reconnectCounter, error);
		g_reconnectCounter++;
		ConnectSQL();
		
		return;
	}
	
	char driver[16];
	SQL_GetDriverIdent(owner, driver, sizeof(driver));
	
	g_hSQL = CloneHandle(hndl);
	
	if (StrEqual(driver, "mysql", false))
	{
		SQL_TQuery(g_hSQL, CreateSQLTableCallback, "CREATE TABLE IF NOT EXISTS `fightzones` (`id` int(11) NOT NULL AUTO_INCREMENT, `type` int(11) NOT NULL, `level_id` int(11) NOT NULL, `point1_x` float NOT NULL, `point1_y` float NOT NULL, `point1_z` float NOT NULL, `point2_x` float NOT NULL, `point2_y` float NOT NULL, `point2_z` float NOT NULL, `map` varchar(64) NOT NULL, `name` varchar(32) NOT NULL, PRIMARY KEY (`id`));");
	}
	else if (StrEqual(driver, "sqlite", false))
	{
		SQL_TQuery(g_hSQL, CreateSQLTableCallback, "CREATE TABLE IF NOT EXISTS `fightzones` (`id` INTEGER PRIMARY KEY, `type` INTEGER NOT NULL, `level_id` INTEGER NOT NULL, `point1_x` float NOT NULL, `point1_y` float NOT NULL, `point1_z` float NOT NULL, `point2_x` float NOT NULL, `point2_y` float NOT NULL, `point2_z` float NOT NULL, `map` varchar(32) NOT NULL, `name` varchar(32) NOT NULL);");
	}
	
	g_reconnectCounter = 1;
}

public CreateSQLTableCallback(Handle owner, Handle hndl, const char[] error, any data)
{
	if (owner == null)
	{
		g_reconnectCounter++;
		ConnectSQL();
		
		return;
	}
	
	if (hndl == null)
		return;
	
	LoadMapZones();
}

void RestartMapZoneEditor(int client)
{
	g_mapZoneEditors[client].Step = 0;
	
	for (int i = 0; i < 3; i++)
		g_mapZoneEditors[client].Point1[i] = 0.0;
	
	for (int i = 0; i < 3; i++)
		g_mapZoneEditors[client].Point1[i] = 0.0;		
}

void DisplaySelectPointMenu(int client, int n)
{
	Handle panel = CreatePanel();
	
	char message[255];
	
	FormatEx(message, sizeof(message), "Point Select Panel. Press Mouse2 to set point %d", n);
	
	DrawPanelItem(panel, message, ITEMDRAW_RAWLINE);
	
	FormatEx(message, sizeof(message), "Cancel");
	DrawPanelItem(panel, message);
	
	SendPanelToClient(panel, client, PointSelect, 540);
	CloseHandle(panel);
}

void DisplayPleaseWaitMenu(int client)
{
	Handle panel = CreatePanel();
	
	char wait[64];
	FormatEx(wait, sizeof(wait), "Please wait");
	DrawPanelItem(panel, wait, ITEMDRAW_RAWLINE);
	
	SendPanelToClient(panel, client, PointSelect, 540);
	CloseHandle(panel);
}

void DisplayNoMenu(int client)
{
	Handle panel = CreatePanel();
	
	char wait[64];
	FormatEx(wait, sizeof(wait), "");
	DrawPanelItem(panel, wait, ITEMDRAW_RAWLINE);
	
	SendPanelToClient(panel, client, PointSelect, 540);
	CloseHandle(panel);
}

public int PointSelect(Handle menu, MenuAction action, param1, param2)
{
	if (action == MenuAction_End) 
	{
		CloseHandle(menu);
	} 
	else if (action == MenuAction_Select) 
	{
		if (param2 == MenuCancel_Exit) 
		{
			DisplaySelectPointMenu(param1, 1);
		}
		
		RestartMapZoneEditor(param1);
	}
}

public Action ChangeStep(Handle timer, any serial)
{
	int client = GetClientFromSerial(serial);
	
	if(g_mapZoneEditors[client].Step == 0)
		return Plugin_Continue;
	
	g_mapZoneEditors[client].Step = 2;
	CreateTimer(0.1, DrawAdminBox, GetClientSerial(client), TIMER_REPEAT);
	
	DisplaySelectPointMenu(client, 2);
	
	return Plugin_Continue;
}

public Action DrawAdminBox(Handle timer, any serial)
{
	int client = GetClientFromSerial(serial);
	
	if (g_mapZoneEditors[client].Step == 0)
	{
		return Plugin_Stop;
	}
	
	float a[3], b[3];
	
	Array_Copy(g_mapZoneEditors[client].Point1, b, 3);
	
	if (g_mapZoneEditors[client].Step == 3)
		Array_Copy(g_mapZoneEditors[client].Point2, a, 3);
	else
	GetClientAbsOrigin(client, a);
	
	int color[4] = {255, 255, 255, 255};
	
	a[2]=a[2]+100;
	DrawBox(a, b, 0.1, color, false);
	return Plugin_Continue;
}

void DrawBox(float fFrom[3], float fTo[3], float fLife, color[4], bool:flat)
{
	fFrom[2] += 4.0;
	
	//initialize tempoary variables bottom front
	float fLeftBottomFront[3];
	fLeftBottomFront[0] = fFrom[0];
	fLeftBottomFront[1] = fFrom[1];
	if(flat)
		fLeftBottomFront[2] = fTo[2];
	else
		fLeftBottomFront[2] = fTo[2];
	
	float fRightBottomFront[3];
	fRightBottomFront[0] = fTo[0];
	fRightBottomFront[1] = fFrom[1];
	if(flat)
		fRightBottomFront[2] = fTo[2];
	else
		fRightBottomFront[2] = fTo[2];
	
	//initialize tempoary variables bottom back
	float fLeftBottomBack[3];
	fLeftBottomBack[0] = fFrom[0];
	fLeftBottomBack[1] = fTo[1];
	if(flat)
		fLeftBottomBack[2] = fTo[2];
	else
		fLeftBottomBack[2] = fTo[2];
	
	float fRightBottomBack[3];
	fRightBottomBack[0] = fTo[0];
	fRightBottomBack[1] = fTo[1];
	if(flat)
		fRightBottomBack[2] = fTo[2];
	else
		fRightBottomBack[2] = fTo[2];
	
	//initialize tempoary variables top front
	float lefttopfront[3];
	lefttopfront[0] = fFrom[0];
	lefttopfront[1] = fFrom[1];
	if(flat)
		lefttopfront[2] = fFrom[2];
	else
		lefttopfront[2] = fFrom[2];
	float righttopfront[3];
	righttopfront[0] = fTo[0];
	righttopfront[1] = fFrom[1];
	if(flat)
		righttopfront[2] = fFrom[2];
	else
		righttopfront[2] = fFrom[2];
	
	//initialize tempoary variables top back
	float fLeftTopBack[3];
	fLeftTopBack[0] = fFrom[0];
	fLeftTopBack[1] = fTo[1];
	if(flat)
		fLeftTopBack[2] = fFrom[2];
	else
		fLeftTopBack[2] = fFrom[2];
	float fRightTopBack[3];
	fRightTopBack[0] = fTo[0];
	fRightTopBack[1] = fTo[1];
	if(flat)
		fRightTopBack[2] = fFrom[2];
	else
	fRightTopBack[2] = fFrom[2];
	
	//create the box
	TE_SetupBeamPoints(lefttopfront,righttopfront,precache_laser,0,0,0,fLife,3.0,1.0,10,0.0,color,0);TE_SendToAll(0.0);
	TE_SetupBeamPoints(lefttopfront,fLeftTopBack,precache_laser,0,0,0,fLife,3.0,1.0,10,0.0,color,0);TE_SendToAll(0.0);
	TE_SetupBeamPoints(fRightTopBack,fLeftTopBack,precache_laser,0,0,0,fLife,3.0,1.0,10,0.0,color,0);TE_SendToAll(0.0);
	TE_SetupBeamPoints(fRightTopBack,righttopfront,precache_laser,0,0,0,fLife,3.0,1.0,10,0.0,color,0);TE_SendToAll(0.0);
	
	if(!flat)
	{
		TE_SetupBeamPoints(fLeftBottomFront,fRightBottomFront,precache_laser,0,0,0,fLife,3.0,1.0,0,0.0,color,0);TE_SendToAll(0.0);
		TE_SetupBeamPoints(fLeftBottomFront,fLeftBottomBack,precache_laser,0,0,0,fLife,3.0,1.0,0,0.0,color,0);TE_SendToAll(0.0);
		TE_SetupBeamPoints(fLeftBottomFront,lefttopfront,precache_laser,0,0,0,fLife,3.0,1.0,0,0.0,color,0);TE_SendToAll(0.0);
		
		TE_SetupBeamPoints(fRightBottomBack,fLeftBottomBack,precache_laser,0,0,0,fLife,3.0,1.0,0,0.0,color,0);TE_SendToAll(0.0);
		TE_SetupBeamPoints(fRightBottomBack,fRightBottomFront,precache_laser,0,0,0,fLife,3.0,1.0,0,0.0,color,0);TE_SendToAll(0.0);
		TE_SetupBeamPoints(fRightBottomBack,fRightTopBack,precache_laser,0,0,0,fLife,3.0,1.0,0,0.0,color,0);TE_SendToAll(0.0);
		
		TE_SetupBeamPoints(fRightBottomFront,righttopfront,precache_laser,0,0,0,fLife,3.0,1.0,0,0.0,color,0);TE_SendToAll(0.0);
		TE_SetupBeamPoints(fLeftBottomBack,fLeftTopBack,precache_laser,0,0,0,fLife,3.0,1.0,0,0.0,color,0);TE_SendToAll(0.0);
	}
}

void DeleteAllZoneEntitys()
{
	for (int i = MaxClients; i < 2047; i++)
	{
		if(!IsValidEntity(i)) continue;
		
		if(g_MapZoneEntityZID[i] == -1) continue;
		
		char EntName[256];
		Entity_GetName(i, EntName, sizeof(EntName));
		
		int valid2 = StrContains(EntName, "#FIGHTZONE");
		if(valid2 > -1)
		{
			SDKUnhook(i, SDKHook_StartTouch, StartTouchTrigger);
			SDKUnhook(i, SDKHook_EndTouch, EndTouchTrigger);
			DeleteEntity(i);
		}
		
		for (int client = 1; client <= MaxClients; client++)
		{
			g_bZone[g_MapZoneEntityZID[i]][client] = false;
		}
		
		g_MapZoneEntityZID[i] = -1;
	}
}

void DeleteEntity(int entity)
{
	AcceptEntityInput(entity, "kill");
}

void SpawnZoneEntitys(int zone)
{
	if(g_mapZones[zone].Point1[0] != 0.0 || g_mapZones[zone].Point1[1]  != 0.0 || g_mapZones[zone].Point1[2] != 0.0 )
	{
		int entity = CreateEntityByName("trigger_multiple");
		if (entity > 0)
		{
			if(!IsValidEntity(entity))
				return;
			
			g_MapZoneEntityZID[entity] = zone;
			
			SetEntityModel(entity, "models/props_junk/wood_crate001a.mdl"); 
			
			float origin[3];
			origin[0] = (g_mapZones[zone].Point1[0] + g_mapZones[zone].Point2[0]) / 2.0;
			origin[1] = (g_mapZones[zone].Point1[1] + g_mapZones[zone].Point2[1]) / 2.0;
			origin[2] = g_mapZones[zone].Point1[2] / 1.0;
			
			float minbounds[3]; 
			float maxbounds[3]; 
			
			minbounds[0] = FloatAbs(g_mapZones[zone].Point1[0]-g_mapZones[zone].Point2[0]) / -2.0;
			minbounds[1] = FloatAbs(g_mapZones[zone].Point1[1]-g_mapZones[zone].Point2[1]) / -2.0;
			minbounds[2] = -1.0;
			
			maxbounds[0] = FloatAbs(g_mapZones[zone].Point1[0]-g_mapZones[zone].Point2[0]) / 2.0;
			maxbounds[1] = FloatAbs(g_mapZones[zone].Point1[1]-g_mapZones[zone].Point2[1]) / 2.0;
			maxbounds[2] = FloatAbs(g_mapZones[zone].Point1[2]-g_mapZones[zone].Point2[2]) / 1.0;
			
			//Resize trigger
			minbounds[0] += 16.0;
			minbounds[1] += 16.0;
			minbounds[2] += 16.0;
			
			maxbounds[0] -= 16.0;
			maxbounds[1] -= 16.0;
			maxbounds[2] -= 16.0;
			
			if(IsValidEntity(entity))
			{
				DispatchKeyValue(entity, "spawnflags", "257"); 
				DispatchKeyValue(entity, "StartDisabled", "0");
				DispatchKeyValue(entity, "OnTrigger", "!activator,IgnitePlayer,,0,-1");
				
				char EntName[256];
				FormatEx(EntName, sizeof(EntName), "#FIGHTZONE_%d", g_mapZones[zone].Id);
				DispatchKeyValue(entity, "targetname", EntName);
				
				if(DispatchSpawn(entity))
				{
					ActivateEntity(entity);
					
					SetEntPropVector(entity, Prop_Send, "m_vecMins", minbounds);
					SetEntPropVector(entity, Prop_Send, "m_vecMaxs", maxbounds);
					
					SetEntProp(entity, Prop_Send, "m_nSolidType", 2);
					
					TeleportEntity(entity, origin, NULL_VECTOR, NULL_VECTOR);
					
					// SetVariantString(Buffer);
					AcceptEntityInput(entity, "SetParent");
					
					int iEffects = GetEntProp(entity, Prop_Send, "m_fEffects");
					iEffects |= 0x020;
					SetEntProp(entity, Prop_Send, "m_fEffects", iEffects);
					
					SDKHookEx(entity, SDKHook_StartTouch, StartTouchTrigger);
					SDKHookEx(entity, SDKHook_EndTouch, EndTouchTrigger);
					
					int color[4] = {0, 0, 255, 255};
					
					float a[3], b[3];
					a[0] = g_mapZones[zone].Point1[0];
					a[1] = g_mapZones[zone].Point1[1];
					a[2] = g_mapZones[zone].Point1[2];
					
					b[0] = g_mapZones[zone].Point2[0];
					b[1] = g_mapZones[zone].Point2[1];
					b[2] = g_mapZones[zone].Point2[2];
					
					DrawBox(a, b, 3.0, color, true);
				}
			}
		}
	}
}

public Action Command_ReloadZones(int client, int args)
{
	LoadMapZones();
	PrintToChat(client, "Zones reloaded");
	return Plugin_Handled;
}

public Action Command_DeleteZone(int client, int args)
{
	DeleteMapZone(client);
	return Plugin_Handled;
}

void DeleteMapZone(int client)
{
	float vec[3];
	GetClientAbsOrigin(client, vec);
	
	bool found = false;
	
	for (int zone = 0; zone < g_mapZonesCount; zone++)
	{
		if (IsInsideBox(vec, g_mapZones[zone].Point1[0], g_mapZones[zone].Point1[1], g_mapZones[zone].Point1[2], g_mapZones[zone].Point2[0], g_mapZones[zone].Point2[1], g_mapZones[zone].Point2[2]))
		{
			char query[64];
			FormatEx(query, sizeof(query), "DELETE FROM fightzones WHERE id = %d", g_mapZones[zone].Id);
			
			SQL_TQuery(g_hSQL, DeleteMapZoneCallback, query, client, DBPrio_Normal);
			
			for (int c = 1; c <= MaxClients; c++)
				g_bZone[zone][c] = false;
				
			PrintToChat(client, "Zone deleted.");
			
			found = true;
			break;
		}
	}
	
	if(!found) 
		PrintToChat(client, "No zone found, you have to stay inside a zone to delete it.");
}

public DeleteMapZoneCallback(Handle owner, Handle hndl, const char[] error, any client)
{
	if (hndl == null)
		return;
	
	PrintToChat(client, "Zone deleted. Reloading zones.");
	LoadMapZones();
}

bool IsInsideBox(float fPCords[3], float fbsx, float fbsy, float fbsz, float fbex, float fbey, float fbez)
{
	float fpx = fPCords[0];
	float fpy = fPCords[1];
	float fpz = fPCords[2]+30;
	
	bool bX = false;
	bool bY = false;
	bool bZ = false;
	
	if (fbsx > fbex && fpx <= fbsx && fpx >= fbex)
		bX = true;
	else if (fbsx < fbex && fpx >= fbsx && fpx <= fbex)
		bX = true;
	
	if (fbsy > fbey && fpy <= fbsy && fpy >= fbey)
		bY = true;
	else if (fbsy < fbey && fpy >= fbsy && fpy <= fbey)
		bY = true;
	
	if (fbsz > fbez && fpz <= fbsz && fpz >= fbez)
		bZ = true;
	else if (fbsz < fbez && fpz >= fbsz && fpz <= fbez)
		bZ = true;
	
	if (bX && bY && bZ)
		return true;
	
	return false;
}

bool IsPlayerTouchingZoneType(int client, int type)
{
	for (int i = 0; i < g_mapZonesCount; i++)
	{
		if(g_mapZones[i].Type != type)
			continue;

		if(g_bZone[i][client])
			return true;
	}

	return false;
}

int g_iEqWeaponType[MAXPLAYERS+1];

public Action Event_ItemEquip(Handle event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	g_iEqWeaponType[client] = GetEventInt(event, "weptype");
}

public Action OnPlayerRunCmd(client, &buttons, &impulse, float vel[3], float angles[3], &weapon)
{
	if (!IsPlayerAlive(client) || IsClientSourceTV(client))
		return Plugin_Continue;
		
	if(!g_bAllowShooting[client] && g_iEqWeaponType[client] != 0)
	{
		float time = GetGameTime();
		SetEntPropFloat(client, Prop_Send, "m_flNextAttack", time+0.5);
		
		if(weapon > 0 && IsValidEntity(weapon))
			SetEntPropFloat(weapon, Prop_Send, "m_flNextPrimaryAttack", time+0.5);
	}
		
	if(g_mapZoneEditors[client].Step == 0)
		return Plugin_Continue;
	
	if (buttons & IN_ATTACK2)
	{
		if (g_mapZoneEditors[client].Step == 1)
		{
			PrintCenterText(client, "Point 1");
			float vec[3];			
			GetClientAbsOrigin(client, vec);
			g_mapZoneEditors[client].Point1 = vec;
			DisplayPleaseWaitMenu(client);
			CreateTimer(1.0, ChangeStep, GetClientSerial(client));
			return Plugin_Handled;
		}
		else if (g_mapZoneEditors[client].Step == 2)
		{
			PrintCenterText(client, "Point 2");
			float vec[3];
			GetClientAbsOrigin(client, vec);
			g_mapZoneEditors[client].Point2 = vec;
			g_mapZoneEditors[client].Step = 3;
			
			float point1[3];
			Array_Copy(g_mapZoneEditors[client].Point1, point1, 3);
			
			float point2[3];
			Array_Copy(g_mapZoneEditors[client].Point2, point2, 3);
			
			point1[2] -= 2;
			point2[2] += 100;
			
			char sName[32];
			
			if(g_mapZoneEditors[client].Type == ZONETYPE_SHOOT)
				Format(sName, 32, "Shoot");
			else if(g_mapZoneEditors[client].Type == ZONETYPE_DMG)
				Format(sName, 32, "Knife");
			else Format(sName, 32, "Other");
			
			AddMapZone(g_currentMap, g_mapZoneEditors[client].Type, sName, 0, point1, point2);
			
			DisplayNoMenu(client);
			PrintToChat(client, "Zone created");
			
			RestartMapZoneEditor(client);
			LoadMapZones();
			
			g_mapZoneEditors[client].Step = 0;
			
			return Plugin_Handled;
		}		
	}
	
	return Plugin_Continue;
}

public Action Hook_OnTakeDamage(victim, &attacker, &inflictor, &Float:damage, &damagetype)
{
	if(!g_bAllowDamage[victim])
		return Plugin_Handled;
		
	return Plugin_Continue;
}
